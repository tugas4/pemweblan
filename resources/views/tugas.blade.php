@extends('coba')

@section('title', 'Daftar Tugas')

@section('container')

<div class="container">
    <img src="{{ URL::asset('images/star.jpg') }}" alt="">
    <div class="teks2">
        <h1>To-do list pemweb-lanjut.</h1>
        @foreach ($tugas as $tugass)

        <p><strong>Tugas {{ $tugass['tugas']}}</strong></p>
        <p> --- {{ $tugass['subject']}}</p>

        @endforeach

    </div>
</div>
<!-- 
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Tugas Pemweb Lanjut</h1>

            @foreach ($tugas as $tugass)

            <p><strong>Tugas {{ $tugass['tugas']}}</strong></p>
            <p> Bab {{ $tugass['subject']}}</p>

            @endforeach
        </div>
    </div>
</div> -->
@endsection
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ URL::asset('css/tampilan.css') }}">
</head>

<nav>
    <input id="nav-toggle" type="checkbox">
    <div class="logo">WEBNYA<strong>NUREKA.</strong></div>
    <ul class="links">
        <li><a href="/">Home</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="/tugas">To-do List</a></li>
    </ul>
    <label for="nav-toggle" class="icon-burger">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </label>
</nav>

@yield('container')
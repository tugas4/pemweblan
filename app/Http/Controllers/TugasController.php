<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class TugasController extends Controller
{
    public function index()
    {

        $tugas = [
            ['tugas' => 'pertama', 'subject' => 'Instalasi dan membuat project laravel'],
            ['tugas' => 'kedua', 'subject' => 'Membuat halaman hello world'],
            ['tugas' => 'ketiga', 'subject' => 'Membuat halaman dengan menggunkan fitur blade'],
        ];

        return view('tugas', compact('tugas'));
    }
}
